﻿const path = require('path');

const webpack = require("webpack"); 
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var HtmlWebpackPlugin = require('html-webpack-plugin');
const p = {};
p.base = path.resolve(__dirname);
p.src = path.resolve(p.base, 'src');
p.entry = path.resolve(p.src, 'app.js');
p.out = path.resolve(p.base, 'build');

const NODE_ENV = process.env.NODE_ENV || "local";
const IS_DEV = NODE_ENV == 'local';

const config = {
    entry: [p.entry],
    output: {
        path: p.out,
        filename: 'bundle-[chunkhash].js',
        publicPath: !IS_DEV? '/build/' : 'http://localhost:3010/'
    },
    devServer: {
        port: 3010,
        //resolve cross-domain error with font-awesome css file during DEV
        headers: { "Access-Control-Allow-Origin": "*" }
    },
    resolve: {
        extensions: ['.js', '.vue', '.css'],
        //resolve the following issue with vuejs-datepicker Vue Warning
        //You are using the runtime-only build of Vue where the template option is not available
        alias: { vue: 'vue/dist/vue.js' }
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: "css-loader"
                })
                
            },
            {
                test: /\.(png|jpg|gif)$/,
                loader: 'url-loader',
                options: {
                    limit: 10000
                }
            },
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: "url-loader?limit=10000&mimetype=application/font-woff"
            },
            {
                test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: "file-loader"
            }

        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            GLOBAL: JSON.stringify({ "ENV": NODE_ENV })
        }),
        new ExtractTextPlugin({
            filename: "style.css" 
        }),
		new HtmlWebpackPlugin({
			template: "src/index.html"
		})
    ]
}

module.exports = config;