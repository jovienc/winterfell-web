﻿import * as configuration from '../config'
import axios from 'axios'

var featureMixins = {
    data () {
        return {
            parameters: {
                apiUrl: configuration.get("apiUrl"),
                apiRouteList: '/winterfell/features'
            },
            element: null,
            id: null,
            projectId: null
        }
    },
    mounted() {
        this.id = this.$route.params.id;
        if (this.id) {
            var options = { headers: {} };
            options.headers[configuration.get("apiHeaderToken")] = configuration.get("apiToken");

            axios.get(this.parameters.apiUrl + this.parameters.apiRouteList + '/' + this.id, options)
                .then(response => {
                    console.log(response.data.result);
                    this.element = response.data.result;
                });
        } else {
            this.projectId = this.$route.params.projectId;
            this.element = this.templateElement;
        }
    },
    methods: {
        update() {

            var isValid = false;
            if (this.$v && this.$v.element) {
                if (!this.$v.element.$invalid) {
                    isValid = true;
                } else {
                    this.$v.element.$touch();
                }
            } else {
                isValid = true;
            }

            if (isValid) {
                var options = { headers: {} };
                options.headers[configuration.get("apiHeaderToken")] = configuration.get("apiToken");

                axios.post(this.parameters.apiUrl + this.parameters.apiRouteList + '/', this.element, options)
                    .then(response => {
                        if (response.data.result) {
                            this.$toasted.success('Saved', { duration: 2000 });
                        } else {
                            this.$toasted.error('Unable to save form.', { duration: 5000 });
                        }
                    });
            }
        }
    }
    
};

export default featureMixins;