﻿import Vue from 'vue';
import Vuelidate from 'vuelidate';
import Toasted from 'vue-toasted';
import BootstrapVue from 'bootstrap-vue';
import Billing from './billing/billing';
import BillingListing from './billing/billingListing';
import ReIndex from './backend/reindex.vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import './css/site.css';
import 'element-ui/lib/theme-default/index.css';
import './css/vue-multiselect/vue-multiselect.min.css';


Vue.use(BootstrapVue);
Vue.use(Vuelidate);
Vue.use(Toasted);

function Boot(component, element) {
    var Compo = Vue.extend(component);
    return new Compo({
        el: element
    });
}

export {
    Vue, Boot, BootstrapVue, ReIndex, BillingListing, Billing
};