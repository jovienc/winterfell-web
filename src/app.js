﻿// Components.
import Vue from 'vue';
import Vuelidate from 'vuelidate';
import VueRouter from 'vue-router'
import Toasted from 'vue-toasted';
import BootstrapVue from 'bootstrap-vue';
import lodash from 'lodash'
import VueLodash from 'vue-lodash/dist/vue-lodash.min'
import VueClip from 'vue-clip'

import Layout from './layout.vue'
import ReIndex from './backend/reindex.vue'

import Home from './global/home';
import ReIndex from './backend/reindex';
import Billing from './billing/billing';
import BillingListing from './billing/billingListing';
import ProjectListing from './project/projectListing';
import Project from './project/project';
import CampaignListing from './campaign/campaignListing';
import CampaignCreate from './campaign/campaignCreate';
import Campaign from './campaign/campaign';
import Hosting from './global/hosting';
import QuatsenQuat from './global/notFound';

// CSS.
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import './css/site.css';
import 'element-ui/lib/theme-default/index.css';
import './css/vue-multiselect/vue-multiselect.min.css';

// Use.
Vue.use(BootstrapVue);
Vue.use(Vuelidate);
Vue.use(Toasted);
Vue.use(VueClip);
Vue.use(VueRouter);
Vue.use(VueLodash, lodash);

const routes = [
{
    name: 'Not found',
    path: '*',
    component: QuatsenQuat
},
{
    name: 'home',
    path: '/',
    component: Home
},
{
    path: '/billing',
    component: BillingListing
},
{
    path: '/billing/new',
    component: Billing
},
{
    path: '/run/campaigns',
    component: CampaignListing
},
{
    name: 'campaignCreate',
    path: '/run/campaign/create',
    component: CampaignCreate
},
{
    name: 'campaign',
    path: '/run/campaign/:campaignId',
    component: Campaign
},
{
    path: '/run/projects',
    component: ProjectListing
},
{
    name: 'project',
    path: '/run/project/:projectId',
    component: Project
},
{
    name: 'featureEdit',
    path: '/run/feature/edit/:id',
    component: Hosting
},
{
    name: 'featureAdd',
    path: '/run/feature/add/:projectId',
    component: Hosting
},
{
    path: '/backend/reindex',
    component: ReIndex

}]

const router = new VueRouter({
    routes: routes
})

const app = new Vue({
    render: h => h(Layout),
    router
}).$mount('#app');